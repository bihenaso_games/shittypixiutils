var app = new PIXI.Application(800, 600, {background: 0x000000, antialias: true});

var timer = new PixiStopWatch(90, 90, 60, {"animStyle" : "cursor", "colorAnim" : false});
timer.drawBack();
timer.run(70);

var timer1 = new PixiStopWatch(90, 220, 60, {"animStyle" : "fluid", "colorAnim" : false});
timer1.drawBack();
timer1.run(70);

var timer2 = new PixiStopWatch(220, 90, 60, {"animStyle" : "cursor", "colorAnim" : true});
timer2.drawBack();
timer2.run(70);

var timer3 = new PixiStopWatch(220, 220, 60, {"animStyle" : "fluid", "colorAnim" : true});
timer3.drawBack();
timer3.run(70);

app.stage.addChild(timer);
app.stage.addChild(timer1);
app.stage.addChild(timer2);
app.stage.addChild(timer3);

window.onload = function(){
	document.body.appendChild(app.view);
}
