class PixiStopWatch extends PIXI.Container{
	constructor(x, y, radius, properties = {}){
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.pivot.x = this.x + this.radius;
		this.pivot.y = this.y + this.radius;
		this.properties = properties;
		this.second = 0;
		this.minute = 0;
		this.hour = 0;
		this.angle = 0;
		this.intervalID = 0;
		this.intervalID1 = 0;
		this.intervalID2 = 0;
		this._angleConst = 6;
		this.interColor = 0;
		this.setProperties();

	}
	setProperties(){
		
		this.backColor = !this.properties.backColor == 1 ? 0xdcdcdc : this.properties.backColor;
		this.frontColor = !this.properties.frontColor == 1 ? 0x696969 : this.properties.frontColor;
		this.animColor  = !this.properties.animColor == 1 ? 0xE53A1B : this.properties.animColor;
		this.interColor = this.frontColor;
		console.log(this.animColor);
		
		this.backSize = !this.properties.backSize == 1 ? 3 : this.properties.backSize;
		this.frontSize = !this.properties.frontSize == 1 ? 6 : this.properties.frontSize;
		
		this.fontType = !this.properties.fontType == 1 ? "Courier New" : this.properties.fontType;
		this.fontColor = !this.properties.fontColor == 1 ? "0xdcdcdc" : this.properties.fontColor;
		
		this.textStyle = new PIXI.TextStyle( {
			fontFamily: this.fontType,
			fill: this.fontColor,
			fontSize: Math.ceil(this.radius / 4) + "px",
			align: "center"
		});
		
		this.animStyle = !this.properties.animStyle == 1 ? "cursor" : this.properties.animStyle;
		this.colorAnim = !this.properties.colorAnim == 1 ? false : this.properties.colorAnim;
		this._animStep = this.animStyle == "cursor" ? 1 : 30;
		this._graphicsStep = this._angleConst / this._animStep;
		this.colorStep = Math.ceil((this.animColor - this.frontColor) / 10);
	}
	drawBack(){
		this.backGraphics = new PIXI.Graphics();
		this.frontGraphics = new PIXI.Graphics();
		
		this.backGraphics.lineStyle(this.backSize, this.backColor, 0.7);
		this.backGraphics.arc(this.x + this.radius, this.y + this.radius, this.radius, 0, 2 * Math.PI);
		this.addChild(this.backGraphics);
		this.addChild(this.frontGraphics);
		
		this.text = new PIXI.Text("00:00:00", this.textStyle);
		this.text.x = this.x + this.radius;
		this.text.y =  this.y + this.radius;
		this.text.anchor.set(0.5);
		this.addChild(this.text);
	}
	drawFront(before, now){
		this.frontGraphics.clear();
		this.frontGraphics.lineStyle(this.frontSize, this.interColor, 1);
		this.drawArc(0, now % 360);		
	}
	
	drawArc(before, step, count){
		this.frontGraphics.arc(this.x + this.radius, this.y + this.radius, this.radius, degToRad(before), degToRad(before+step));
	}
	run(sec = 60, callback = function(){return;} ){
		this.intervalID = setInterval(function(){
							this.updateSec(sec, callback);
						}.bind(this), 1000);
		this.intervalID1 = setInterval(function(){
							this.updateFront(sec, callback);
						}.bind(this), 1000 / this._animStep);
		if(this.colorAnim){
			this.intervalID2 = setInterval(function(){
								this.updateLineProperties(sec, callback);
							}.bind(this), 6000);
		}
	}
	updateSec(sec = 60, callback){
		if(this.second == sec){
			clearInterval(this.intervalID);
			this.second = 0;
			callback();
		}else{
			this.second++;
			this.minute = Math.floor(this.second / 60);
			this.hour = Math.floor(this.minute / 60);
			
			this.text.text = setCharAt(this.text.text, 7, this.second % 10);
			this.text.text = setCharAt(this.text.text, 6, Math.floor(this.second / 10) % 6);
			this.text.text = setCharAt(this.text.text, 4, this.minute % 10);
			this.text.text = setCharAt(this.text.text, 3, Math.floor(this.minute / 10) % 6);
			this.text.text = setCharAt(this.text.text, 1, this.hour % 10);
			this.text.text = setCharAt(this.text.text, 0, Math.floor(this.hour / 10));
		}
	
	}
	updateFront(sec, callback){
		if(this.second == sec){
			clearInterval(this.intervalID1);
			this.frontGraphics.clear();
			this.angle == 0;
			callback();
		}else{
			var before = this.angle;
			this.angle += (6 / this._animStep);
			this.drawFront(before, this.angle);
		}	
	}
	updateLineProperties(sec, callback){
		if(this.second == sec){
			clearInterval(this.intervalID2);
			callback();
		}else if(this.second % 60 == 0){
			this.interColor = this.frontColor;
		}else{
			this.interColor = this.interColor + this.colorStep;
		}		
	}
}

function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}
function radToDeg(rad){
  var pi = Math.PI;
  return Math.ceil(rad * (180/pi));
}
function degToRad(deg){
	var pi = Math.PI;
	return (deg * pi) / 180;
}
function decimalToHexString(number){

  return "0x" + number.toString(16).toUpperCase();
}
